<script>

    let orderModal = $('#create-order-modal');
    let itemModal = $('#create-item-modal');
    let itemOrderToggle = $('#order-item-collapse');

    let initOrderModal = function() {
        let select = orderModal.find('#item-select');
        $.ajax({
            url: 'api/items',
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                response.forEach((item) => {
                   let option = $('<option ' +
                       'data-price="' + item.price + '" ' +
                       'data-name="'+ item.name  + '"' +
                       'value="' + item.id + '"> ' +
                       'Name ( ' + item.name + ') Price : ' + item.price +
                       '</option>');
                   select.append(option);
                });
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });
    };

    let addItem = function () {
        let quantity  = itemOrderToggle
            .find('#quantity')
            .val();

        let name  = itemOrderToggle
            .find('#item-select')
            .find(':selected')
            .data('name');
        let price = itemOrderToggle
            .find('#item-select')
            .find(':selected')
            .data('price') * quantity;

        let id = itemOrderToggle.find('#item-select').val();

        itemOrderToggle.find('#quantity').val('');
        itemOrderToggle.find('#item-select').val('void');
        console.log(price);
        let label = $('<h4>'+name+'  <span ' +
            'data-quantity="' + quantity + '" ' +
            'data-id="' + id + '" ' +
            'class="label label-success">Total price   $  ' + price + ' </span></h4>');
        $('#labels-container').append(label);
    };

    let createOrder = function () {
        let items = [];
        $('#labels-container').children('h4').each(function () {
            items.push({
                id : $(this).find('span').data('id'),
                quantity : $(this).find('span').data('quantity')
            });
        });
        let data= {
            items : items,
            street_address_line1: orderModal.find('#address-line1').val(),
            street_address_line2: orderModal.find('#address-line2').val(),
            city: orderModal.find('#city').val(),
            country: orderModal.find('#country').val(),
            state: orderModal.find('#state').val(),
            special_instructions: orderModal.find('#s-instructions').val(),
            zip_code: orderModal.find('#zip-code').val(),
        };
        $.ajax({
            url: 'api/order',
            type: "PUT",
            data : data,
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                alert('Order created');
                location.reload();
            },
            error: function(jqXHR) {
                alert(jqXHR.responseText);
            }
        });
    };

    $( document ).ready(() => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#create-order-btn').on('click',initOrderModal);
        $('#save-order-btn').on('click',createOrder);
        itemOrderToggle.on('click','#add-item-order-btn',addItem);
    });

</script>