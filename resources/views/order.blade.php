@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create order</div>

                <!-- Modal -->
                <div class="modal fade" id="create-order-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Create order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="address-line1">Address line 1</label>
                                        <input type="text" class="form-control" id="address-line1"  placeholder="Adres line1">
                                    </div>
                                    <div class="form-group">
                                        <label for="address-line1">Address line 2</label>
                                        <input type="text" class="form-control" id="address-line2"  placeholder="Adres line2">
                                    </div>
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control" id="city"  placeholder="City">
                                    </div>
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <input type="text" class="form-control" id="country"  placeholder="Country">
                                    </div>
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <input type="text" class="form-control" id="state"  placeholder="State">
                                    </div>
                                    <div class="form-group">
                                        <label for="s-instructions">Special instructions</label>
                                        <input type="text" class="form-control" id="s-instructions"  placeholder="Special instructions">
                                    </div>
                                    <div class="form-group">
                                        <label for="zip-code">Zip code</label>
                                        <input type="number" class="form-control" id="zip-code"  placeholder="Zip code">
                                    </div>

                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a data-toggle="collapse" href="#order-item-collapse">Create item pack</a>
                                                </h6>
                                            </div>
                                            <div id="order-item-collapse" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <form>
                                                        <div class="form-group">
                                                            <label for="sel1">Select item:</label>
                                                            <select class="form-control" id="item-select">
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="quantity">Quantity</label>
                                                            <input type="number" class="form-control" id="quantity"  placeholder="Quantity">
                                                        </div>
                                                        <button data-toggle="collapse" href="#order-item-collapse"
                                                                type="button" class="btn btn-primary" id="add-item-order-btn">
                                                            Add item
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h5>Order packs</h5>
                                            </div>
                                            <div id="order-item-collapse">
                                                <div class="panel-body" id="labels-container">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="save-order-btn">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-12 text-right">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-order-modal" id="create-order-btn">
                            Create order
                        </button>
                    </div>
                        <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Street address 1</th>
                                    <th>Street address 2</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>State</th>
                                    <th>Zip code</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{$order->name}}</td>
                                        <td>{{$order->street_address_line1}}</td>
                                        <td>{{$order->street_address_line2}}</td>
                                        <td>{{$order->city}}</td>
                                        <td>{{$order->country}}</td>
                                        <td>{{$order->state}}</td>
                                        <td>{{$order->zip_code}}</td>
                                        <td>{{$order->itemOrders()->count()}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
   @include('scripts.order')
@endsection
