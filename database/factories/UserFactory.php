<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'sku' => 'huuu3002',
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'price' => $faker->numberBetween(10,13572)
    ];
});

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'id' => 1,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

