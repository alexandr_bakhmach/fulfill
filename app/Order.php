<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = ['id','street_address_line1','street_address_line2',
        'city','state','country','zip_code','phone_number','special_instructions',
        'user_id','shipping_option','created_at','updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function itemOrders()
    {
        return $this->hasMany(ItemOrder::class);
    }
}
