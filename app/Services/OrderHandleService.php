<?php
/**
 * Created by PhpStorm.
 * User: alexandr
 * Date: 26.01.18
 * Time: 14:17
 */

namespace App\Services;


use App\Http\Requests\Api\Order\CreateOrderRequest;
use App\Item;
use App\ItemOrder;
use App\Order;
use App\User;
use Illuminate\Http\JsonResponse;

class OrderHandleService
{
    public function handleOrderCreation(CreateOrderRequest $request,
                                        ShipBobFulfillmentService $shipBobFulfillmentService)
    : JsonResponse
    {

        $user = User::find(1);
        $order = new Order($request->only('street_address_line1','street_address_line2',
            'city','state','country','zip_code','special_instructions'));
        $order->fill([
            'user_id' => $user->id,
            'name' => $user->name,
            'shipping_option' => 1
        ]);
        $order->save();

        $items = $request->get('items');

        foreach ($items as $item) {
            $order->itemOrders()->create([
                'item_id' => $item['id'],
                'quantity' => $item['quantity']
            ]);
        }

    //    $shipBobFulfillmentService->handleOrderCreated($order);

        return response()->json($shipBobFulfillmentService->handleOrderCreated($order));
    }
}