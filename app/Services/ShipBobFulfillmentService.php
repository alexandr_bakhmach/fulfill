<?php
/**
 * Created by PhpStorm.
 * User: alexandr
 * Date: 26.01.18
 * Time: 14:17
 */

namespace App\Services;


use App\Order;

class ShipBobFulfillmentService
{
    public function handleOrderCreated(Order $order)
    {

        $data = [
            'UserToken' => '6a843d55-b441-45ff-8331-dc7c967255d0',
            'Name' => $order->user->name,
            'City' => $order->city,
            'ShippingOption' => $order->shipping_option,
            'StreetAddressLine1' =>  $order->street_address_line1,
            'StreetAddressLine2' => $order->street_address_line2,
            'State' => $order->state,
            'Country' => $order->country,
            'Zipcode' => $order->zip_code,
            'SpecialInstructions' => $order->special_instructions,
            'ReferenceId' => $order->id,
            'InventoryDetails' => []
        ];

        $items = $order->itemOrders()->get();

        foreach ($items as $item) {
            array_push($data['InventoryDetails'],[
                'Sku' => $item->item->sku,
                'Name' => $item->item->name,
                'Quantity'=> $item->quantity
            ]);
        }

        $ch = curl_init();

        $headers = [
            'Accept: application/json',
            'Content-Type: application/json',
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POSTFIELDS,  json_encode($data));


        curl_setopt($ch, CURLOPT_URL, 'http://shipbobapiv2.shipbob.com/api/Orders/PlaceNewWebOrder');

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        curl_setopt($ch, CURLOPT_TIMEOUT, 30);


        $response = curl_exec($ch);

        curl_close($ch);

       return $response;
    }
}