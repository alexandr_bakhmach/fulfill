<?php

namespace App\Http\Requests\Api\Order;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city' =>'required|string|max:80|min:2',
            'country' =>'required|string|max:80|min:2',
            'state' =>'required|string|max:80|min:2',
            'zip_code' =>'required|regex:/\b\d{5}\b/',
            'street_address_line1' => 'required|string|max:150|min:5',
            'street_address_line2' => 'required|string|max:150|min:5',
            'items.*.id' => 'required|exists:items,id',
            'items.*.quantity' => 'required|numeric|min:1|max:99999',
        ];
    }
}
