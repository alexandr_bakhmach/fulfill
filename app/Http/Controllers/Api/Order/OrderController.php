<?php

namespace App\Http\Controllers\Api\Order;

use App\Http\Requests\Api\Order\CreateOrderRequest;
use App\Item;
use App\ItemOrder;
use App\Order;
use App\Services\OrderHandleService;
use App\Services\ShipBobFulfillmentService;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function getOrderInfo(Order $order)
    {
        return response()->json($order);
    }

    public function createOrder(CreateOrderRequest $request,
                                OrderHandleService $orderHandleService,
                                ShipBobFulfillmentService $shipBobFulfillmentService)
    {

        return $orderHandleService
            ->handleOrderCreation($request,$shipBobFulfillmentService);
    }

    public function getOrderItems()
    {
        return response()->json(Item::orderBy('name')->get());
    }
}
