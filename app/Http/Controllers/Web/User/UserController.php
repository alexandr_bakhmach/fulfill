<?php

namespace App\Http\Controllers\Web\User;

use App\Order;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    const PAGINATE_DEFAULT_VALUE = 7;

    public function getOrderPage()
    {
        $orders = Order::with('itemOrders')
            ->orderBy('created_at')
            ->paginate(self::PAGINATE_DEFAULT_VALUE);

        return View('order')->with(compact('orders'));
    }
}
