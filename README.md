# Instalation #

$ git clone <project>

 create .env file and paste all stuff from .env.example to .env
 
 create mysql database with the name like in .env

$ composer install

$ npm install

$ npm run prod

$ php artisan key:generate

$ php artisan migrate

$ php artisan db:seed

$ php artisan serve (Start local server)

